#pragma once
#include <vector>
#include <unordered_map>
#include <iostream>
#include "EntityManagerClass.h"
#include "ComponentManagerClass.h"
#include "RigidbodyClass.h"
#include "TransformClass.h"
#include "MainWindow.h"
#include "Camera.h"

using namespace std;

class Component {
public:
	unordered_map<size_t,size_t> ComponentsSignature;
	unordered_map<string,vector<unordered_map<size_t, void*>>> ComponentsArray;

	Component();

	template<typename T>
	void RegisterComponent();

	template <typename T>
	T* GetComponent(int id);

	size_t GetComponentSize(size_t hash);

	template <typename T>
	void AddComponent(int id);
		
	void RemoveComponent(int id);

	void RemoveComponentsFromScene(string scene);
	//WORK IN PROGRESS
	void CopyComponents(int id, string selected_scene);

	void SetScene(string new_scene);

private:
	string scene;
};


class System {
public:
	EntityManager entityManager;
	Component component;
	ComponentManager cManager;

	Camera camera;

	TransformComponent transformComponent;
	RigidbodyComponent rigidbodyComponent;

	System();

	void CreateEntities(int size);

	void CreateOneEntity();

	void CreateCopyOfEntity(int id, string new_scene);

	void DeleteEntity(int id);

	template<typename T>
	void AddComponentToAllEntities();

	void UpdateEntities();

	void RegisterComponents();

	void SetScene(string selected_scene);

	string GetScene();

	vector<string> GetScenes();

	void DeleteGameobjectsFromScene();

private:
	string scene;

};

extern System systemm;
