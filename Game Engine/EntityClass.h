#pragma once
#include <string>

using namespace std;

class Entity {
public:
	int id;
	string name;
	string description;
	float Time = 0;

	void updateTime(float timestep);
};