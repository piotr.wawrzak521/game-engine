#include "TransformClass.h"

TransformComponent::TransformComponent() {
	format = SDL_PIXELFORMAT_ABGR8888;
}

void TransformComponent::RenderEntity(Transform* transform, glm::vec2 camera_offset) {
	SDL_Rect rect{ transform->pos.x, transform->pos.y, transform->size.x, transform->size.y };

	if (transform->texture_info->texture_name == "") {
		rect.x += camera_offset.x;
		rect.y += camera_offset.y;

		SDL_RenderDrawRect(renderer, &rect);
		SDL_SetRenderDrawColor(renderer, transform->color.x, transform->color.y, transform->color.z, transform->color.a);
		SDL_RenderFillRect(renderer, &rect);
	}

	else {
		BlitTexture(transform, camera_offset);
	}
}

void TransformComponent::BlitTexture(Transform* transform, glm::vec2 camera_offset) {
	SDL_Rect rect;
	
	rect.w = transform->size.x;
	rect.h = transform->size.y;

	rect.x = transform->pos.x + camera_offset.x;
	rect.y = transform->pos.y + camera_offset.y;

	SDL_RenderCopy(renderer, transform->texture_info->texture, NULL, &rect);
}

SDL_Texture* TransformComponent::CreateTexture(string path) {

    SDL_Surface* surface = IMG_Load(path.c_str());

	 SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

	 SDL_FreeSurface(surface);

    return texture;
}

void TransformComponent::LoadTextures() {
	filesystem::path file_path;
	string file_name;
	for (const auto& dirEntry : filesystem::directory_iterator("Assets//Textures")) {
		file_path = dirEntry.path();
		auto file_name = file_path.filename().string();

		if (file_path.extension() == ".png" || file_path.extension() == ".jgp") textures.push_back({ file_name,CreateTexture(file_path.string()) });
	}
}
