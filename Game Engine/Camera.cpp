#include "Camera.h"

Camera::Camera() {
	camera_offset = { 0,0 };
	camera_direction = { 0,0 };
}

void Camera::FollowObject(glm::vec2 objectPosition){
	camera_offset = objectPosition;
}

void Camera::ResetCamera() {
	camera_offset = glm::vec2(0, 0);
}

glm::vec2 Camera::GetCameraOffset() {
	return camera_offset;
}

void Camera::SetCameraDirection(glm::vec2 direction) {
	camera_direction += direction;
}

void Camera::UpdateCamera() {
	camera_offset += camera_direction;
	camera_direction = glm::vec2(0, 0);
}