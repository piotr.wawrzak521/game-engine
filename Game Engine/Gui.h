#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "imguidocking/imgui_impl_sdl.h"
#include "imguidocking/imgui_impl_sdlrenderer.h"
#include "imguidocking/imgui.h"
#include "MainWindow.h"
#include "FileManager.h"
#include "EntityClass.h"
#include "ComponentClass.h"
#include "MapEditor.h"
#include <SDL_image.h>
#include <filesystem>
#include <unordered_map>
#include <iostream>
#include <vector>


struct ObjectParametersStatus {
	string scene;
	bool status;
	int id;
};

struct DirectoriesStatus {
	bool status;
	filesystem::path path;
};

class GUI {
public:
	GUI();

	bool KEYBOARD_ACTIVE{ true };
	bool map_editor_state;
	MapEditor mEditor;
	string scene;
	fileManager fManager;

	void UpdateGui();

private:

	void GameobjectsHierarchy();

	void ObjectsParameters(int id);

	void FilesHierarchy();

	void LoadedGameobjectsHierarchy();

	void Scene();

	void MenuBar();

	filesystem::path current_path;
	vector< filesystem::path> dir_paths;
	ObjectParametersStatus objectsParametersStatus;

	char new_scene_input_buff[16];

	bool scene_window;
	bool objects_parameters_window;
	bool files_hierarchy_window;
	bool gameobjects_hierarchy_window;
	bool loaded_objects_hierarchy_window;

	bool color_picker;

};

extern GUI gui;