#pragma once
#include "MainWindow.h"
#include <glm.hpp>
#include <vector>
#include <SDL.h>
#include <filesystem>
#include <SDL_image.h>

struct TextureInfo {
	string texture_name;
	SDL_Texture* texture;
};

struct Transform {
	glm::vec2 pos; 
	glm::vec2 size; 
	glm::vec4 color; 
	glm::vec2 scale;
	int mass;
	TextureInfo* texture_info;
	//SDL_Texture* texture;

};

class TransformComponent {
public:
	TransformComponent();

	vector<TextureInfo> textures;

	void LoadTextures();

	void RenderEntity(Transform* transform, glm::vec2 camera_offset);

	SDL_Texture* CreateTexture(string path);

	//SDL_Texture* LoadTextureFromPath(string file_name);

	//TextureData GetTextureInfo(SDL_Texture* texture);

	//SDL_Texture* RecreateTextureFromData(char* pixels, TextureData data);

private:
	void BlitTexture(Transform* transform, glm::vec2 camera_offset);

	Uint32 format;
};