#pragma once
#define _CRT_SECURE_NO_DEPRECATE
#include "ComponentClass.h"
#include "string"
#include <fstream>
#include <filesystem>

class fileManager{
public:
	fileManager();

	template<typename T>
	size_t GetComponentSignature();

	void OpenFile(filesystem::path file_path);

	void SaveFile(int id,string name);

	void LoadGameobjects();

private:
	string path;
};