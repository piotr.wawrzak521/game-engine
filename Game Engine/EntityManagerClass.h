#pragma once
#include <vector>
#include <unordered_map>
#include <iostream>
#include "EntityClass.h"

using namespace std;

class EntityManager {
public:
	unordered_map<string,int> eAmount;
	unordered_map<string,vector<Entity>*> eList;
	//Entity entity;
	
	EntityManager();

	void CorrectValues(int id);

	void AddEntity();

	void DeleteEntity(int id);

	void CopyEntity(int id, string selected_scene);

	Entity* GetEntity(int id);
	
	void PrintData();

	void SetScene(string new_scene);

	void DeleteEntitiesFromScene(string scene);
private:
	string scene;
};