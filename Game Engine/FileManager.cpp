#include "FileManager.h"

using namespace std;

fileManager::fileManager() { path = ""; }

template<typename T>
size_t fileManager::GetComponentSignature() {
	const std::type_info& hash_component = typeid(T);
	size_t hash = hash_component.hash_code();
	return hash;
}

std::ifstream::pos_type filesize(const char* filename)
{
	std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
	return in.tellg();
}

void fileManager::LoadGameobjects() {
	filesystem::path file_path;
	string file_name;
	bool exist;
	string saved_scene = systemm.GetScene();
	systemm.SetScene("GUI SCENE");
	systemm.DeleteGameobjectsFromScene();
	for (const auto dirEntry : filesystem::directory_iterator("Assets//Objects")) {
		file_path = dirEntry.path();
		file_name = dirEntry.path().filename().string();
		if (file_path.extension() == ".jebacflorczyka") {
			OpenFile(file_path);
		}
	}
	systemm.SetScene(saved_scene);

}

void fileManager::OpenFile(filesystem::path file_path) {
	std::string file_path_str = file_path.string();
	FILE* fr = fopen(file_path.string().c_str() , "r");
	size_t const_size = filesize(file_path.string().c_str());

	size_t size = 0;
	int texture_lenght;
	//string texture_name;
	string scene = systemm.GetScene();

	Transform* transform_data = new Transform;
	Rigidbody* rigidbody_data = NULL;
	TextureInfo* texture_info = new TextureInfo;

	size_t transformSignature = GetComponentSignature<Transform>();
	size_t rigidbodySignature = GetComponentSignature<Rigidbody>();
	char* buffer = new char[const_size]();
	fread(buffer, const_size, 1, fr);
	memcpy(transform_data, buffer, sizeof(Transform));


	size += sizeof(Transform) - 12;

	memcpy(&texture_lenght, buffer + size, sizeof(int));

	size += sizeof(int);
	if (texture_lenght > 0) {
		//size += sizeof(size_t);
		//buffer_texture
		char* texture_name_buffer = new char[texture_lenght + 1];

		memcpy(texture_name_buffer, buffer + size, texture_lenght + 1);

		texture_info->texture_name = "Assets\\Textures\\";
		texture_info->texture_name += texture_name_buffer;
		texture_info->texture = systemm.transformComponent.CreateTexture(texture_info->texture_name);
		transform_data->texture_info = texture_info;
	}
	else {
		texture_info->texture_name = "";
		transform_data->texture_info = texture_info;
	}
	//START
	unordered_map<size_t, void*> components;

	//
	SDL_Rect viewport;
	SDL_RenderGetViewport(renderer, &viewport);
	transform_data->pos = { (viewport.w/2) - (transform_data->size.x / 2), (viewport.h/2) - (transform_data->size.y/2) };
	//

	size_t tr_size = systemm.component.GetComponentSize(transformSignature);
	void* ptr = new void* [tr_size] {};
	memcpy(ptr, transform_data, sizeof(Transform));
	components.insert({ transformSignature,ptr });

	if (rigidbody_data) {
		size_t rg_size = systemm.component.GetComponentSize(rigidbodySignature);
		void* ptr_rg = new void* [rg_size] {};
		memcpy(ptr_rg, rigidbody_data, sizeof(Rigidbody));
		components.insert({ rigidbodySignature,ptr_rg });
	}
	
	systemm.component.ComponentsArray[scene].push_back(components);
	systemm.entityManager.AddEntity();
	systemm.entityManager.GetEntity(systemm.entityManager.eAmount[scene] - 1)->name = file_path.stem().string();

	fclose(fr);
}

void fileManager::SaveFile(int id,string name) {
	string path = "Assets\\Objects\\" + name + ".jebacflorczyka";
	const char* c_path = path.c_str();
	FILE* fw = fopen(c_path, "w");

	
	Transform* transformValues = systemm.component.GetComponent<Transform>(id);
	size_t transformSignature = GetComponentSignature<Transform>();
	
	Rigidbody* rigidbodyValues = systemm.component.GetComponent<Rigidbody>(id);
	size_t rigidbodySignature = GetComponentSignature<Rigidbody>();

	fwrite(&transformValues->pos.x,8,1,fw);
	fwrite(&transformValues->size.x, 8, 1, fw);
	fwrite(&transformValues->color.x, 16, 1, fw);
	fwrite(&transformValues->scale.x, 8, 1, fw);
	fwrite(&transformValues->mass, sizeof(int), 1, fw);
	//fwrite(&transformValues->texture_info, sizeof(TextureInfo), 1, fw);

	size_t texture_lenght = transformValues->texture_info->texture_name.length();

	fwrite(&texture_lenght, 4, 1, fw);
	if (texture_lenght > 0) {
		transformValues->texture_info->texture_name += '\0';
		fwrite(transformValues->texture_info->texture_name.c_str(), texture_lenght + 1, 1, fw);
		//fwrite(&transformValues->texture_info->texture, sizeof(SDL_Texture*), 1, fw);
	}

	if (rigidbodyValues) {
		cout << rigidbodySignature << endl;
		fwrite(&rigidbodySignature, sizeof(size_t), 1, fw);
		fwrite(&rigidbodyValues->masno, 4, 1, fw);
		fwrite(&rigidbodyValues->masnox, 8, 1, fw);
	}
		
	fclose(fw);
}