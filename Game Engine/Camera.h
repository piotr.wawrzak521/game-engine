#pragma once
#include <vec2.hpp>

class Camera {
public:
	Camera();

	void FollowObject(glm::vec2 objectPosition);

	void SetStaticCamera();

	void ResetCamera();

	void SetCameraDirection(glm::vec2 direction);

	glm::vec2 GetCameraOffset();

	void UpdateCamera();

private:
	int attached_object;
	glm::vec2 camera_direction;
	glm::vec2 camera_offset;
};