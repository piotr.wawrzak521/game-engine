#pragma once
#include "TransformClass.h"
#include "MainWindow.h"

#include <vec2.hpp>
#include <SDL_rect.h>
#include <iostream>

class MapEditor {
public:
	void CreateGrid(SDL_Rect rect);

	void CorrectMousePos(int& mousex,int& mousey);

	void SetGridOffset(glm::vec2 offset);

	glm::vec2 GetGridSize();

	void SetGridSize(glm::vec2 size);

private:
	glm::vec2 grid_size;

	glm::vec2 grid_offset{0,0};
};