#include "ComponentClass.h"

//System system = System();


Component::Component() {
	ComponentsSignature = unordered_map<size_t, size_t>();
	ComponentsArray = unordered_map<string,vector<unordered_map<size_t, void*>>>();
	scene = "0";
}


template<typename T>
void Component::RegisterComponent() {
	const std::type_info& hash_component = typeid(T);
	for (const auto signature : ComponentsSignature) {
		if (hash_component.hash_code() == signature.first) {
			cout << "Component '" << typeid(T).name() << "' already exist" << endl;
			return;
		}
	}
	const size_t size = sizeof(T);
	ComponentsSignature.insert({ hash_component.hash_code(),size });
	cout << hash_component.hash_code() << endl;
	return;
}

size_t Component::GetComponentSize(const size_t hash) {
	for (const auto signature : ComponentsSignature) {
		if (signature.first == hash) return signature.second;
	}
}

template<typename T>
T* Component::GetComponent(const int id) {
	const std::type_info& hash_component = typeid(T);
	for (auto x : ComponentsArray[scene].at(id)) {
		if (x.first == hash_component.hash_code()) return (T*)ComponentsArray[scene].at(id).at(hash_component.hash_code());
	}
	return NULL;
}


template<typename T>
void Component::AddComponent(const int id) {
	void* ptr = new T{};
	const std::type_info& hash_component = typeid(T);
	//Component::RegisterComponent<T>();
	if (ComponentsArray.find(scene) != ComponentsArray.end() && ComponentsArray[scene].size() > id) {
		ComponentsArray[scene][id].insert({ hash_component.hash_code(), ptr });
		return;
	}
	unordered_map<size_t, void*> comp;
	comp.insert({ hash_component.hash_code(), ptr });
	ComponentsArray.emplace(scene, vector<unordered_map<size_t, void*>>{});
	ComponentsArray[scene].push_back(comp);
}

 void Component::CopyComponents(const int id, string selected_scene) {
	unordered_map<size_t, void*> components;
	for (auto x : ComponentsArray[scene].at(id)) {
		const size_t size = GetComponentSize(x.first);
		void* ptr = new void*[size]{};

		memcpy(ptr, x.second, size);
		x.second = ptr;
		components.insert(x);
	}
	ComponentsArray[selected_scene].push_back(components);
}

 void Component::RemoveComponent(const int id) {
	if (ComponentsArray.find(scene) != ComponentsArray.end()) {
		 ComponentsArray[scene].erase(ComponentsArray[scene].begin() + id);
	}
 }

void Component::RemoveComponentsFromScene(const string scene) {
	ComponentsArray[scene].clear();
}


void Component::SetScene(const string new_scene) {
	scene = new_scene;
	if(ComponentsArray[scene].empty()) ComponentsArray[scene] = vector<unordered_map<size_t, void*>>();
}

System::System() {
	scene = "0";

	entityManager = EntityManager();
	component = Component();
	cManager = ComponentManager();
	
	camera = Camera();
	
	transformComponent = TransformComponent();
	rigidbodyComponent = RigidbodyComponent();
	
}

void System::CreateEntities(int size) {
	for (int i = 0; i < size; i++) {
		entityManager.AddEntity();
	}
}

void System::CreateOneEntity() {
	//int mouseX, mouseY;

	SDL_Rect viewport;
	SDL_RenderGetViewport(renderer, &viewport);

	if (!(mousex >= viewport.x && mousex <= viewport.x + viewport.w
		&& mousey >= viewport.y && mousey <= viewport.y + viewport.h)) return;

	entityManager.AddEntity();
	Entity entity = entityManager.eList[scene]->back();
	component.AddComponent<Transform>(entity.id);
	if (entityManager.eList[scene]->back().id % 2 != 0) {
		component.AddComponent<Rigidbody>(entity.id);
		Rigidbody* entityRigidbody = component.GetComponent<Rigidbody>(entity.id);
		entityRigidbody->masno = rand() % 10;
		entityRigidbody->masnox = glm::vec2(3,8);
	}


	//SDL_GetMouseState(&mousex, &mousey);

	//ZAWSZE WPROWADZ KOMPONENT DO KURWY NEDZY
	// np.	Rigidbody* rg = component.GetComponent<Rigidbody>(entity.id);
	///
	Transform* entityTransform = component.GetComponent<Transform>(entity.id);
	entityTransform->pos = { mousex,mousey };
	entityTransform->pos -= camera.GetCameraOffset();
	entityTransform->scale = { 1,1 };
	entityTransform->size = { 32, 32 };
	entityTransform->color = { 100, 100, 100 , 255};
	entityTransform->mass = {24};
	entityTransform->texture_info = new TextureInfo;
}

void System::CreateCopyOfEntity(const int id,const string selected_scene) {
	component.CopyComponents(id, selected_scene);
	entityManager.CopyEntity(id, selected_scene);
}

void System::DeleteEntity(const int id) {
	entityManager.DeleteEntity(id);
	component.RemoveComponent(id);
}


template<typename T>
void System::AddComponentToAllEntities() {
	for (Entity& entity : *entityManager.eList[scene]) {
		component.AddComponent<T>(entity.id);
		component.GetComponent<Transform>(entity.id)->pos = glm::dvec2(rand() % 700, rand() % 700);
		component.GetComponent<Transform>(entity.id)->scale_x = rand() % 50;
		component.GetComponent<Transform>(entity.id)->scale_y = rand() % 50;
	}
}

void System::UpdateEntities() {
	for (Entity& entity : *entityManager.eList[scene]) {

		Transform* entity_transform = component.GetComponent<Transform>(entity.id);

		transformComponent.RenderEntity(entity_transform, camera.GetCameraOffset());

		//entity.updateTime(timestep);
	}
	//cout << entityManager.eAmount << endl;
}

void System::SetScene(const string new_scene) {
	scene = new_scene;
	component.SetScene(scene);
	entityManager.SetScene(scene);
}

string System::GetScene() {
	return scene;
}

//DO ZMIANY
vector<string> System::GetScenes() {
	vector<string> vec;
	for(auto it = systemm.entityManager.eList.begin(); it != systemm.entityManager.eList.end(); ++it) {
		if(it->first != "GUI SCENE") vec.push_back(it->first);
	}
	return vec;
}


void System::DeleteGameobjectsFromScene() {
	entityManager.DeleteEntitiesFromScene(scene);
	component.RemoveComponentsFromScene(scene);
}
/// <summary>
/// TUTAJ DODAJ KURWA KOMPONENTY
/// </summary>

// TO CHANGE

void System::RegisterComponents() {
	component.RegisterComponent<Transform>();
	component.RegisterComponent<Rigidbody>();
}

