#include "MapEditor.h"

void MapEditor::CreateGrid(SDL_Rect rect) {
	for (int y = 0; y < rect.h; y += grid_size.y) {
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderDrawLine(renderer, 0, y - (int)grid_offset.y , rect.w, y - (int)grid_offset.y);
	}

	for (int x = 0; x < rect.w; x += grid_size.x) {
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderDrawLine(renderer, x - (int)grid_offset.x, 0, x - (int)grid_offset.x, rect.h);
	}
}

glm::vec2 MapEditor::GetGridSize() {
	return grid_size;
}

void MapEditor::SetGridSize(glm::vec2 size) {
	grid_size = size;
}

void MapEditor::SetGridOffset(glm::vec2 offset) {
	grid_offset.x = -((int)offset.x % (int)grid_size.x);
	grid_offset.y = -((int)offset.y % (int)grid_size.y);
}

void MapEditor::CorrectMousePos(int& mousex, int& mousey) {
	mousex = ((int)((mousex + grid_offset.x) / grid_size.x) * grid_size.x) - grid_offset.x;
	mousey = ((int)((mousey + grid_offset.y) / grid_size.y) * grid_size.y) - grid_offset.y;
}