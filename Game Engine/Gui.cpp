#include "Gui.h"

using namespace std;

//System systemm;

GUI::GUI() {
	scene = systemm.GetScene();
	current_path = { std::filesystem::current_path() };
	dir_paths = { std::filesystem::current_path() };
	objectsParametersStatus = { scene, false, 0};
	fManager = fileManager();
	mEditor = MapEditor();

	scene_window = { true };
	objects_parameters_window = {true};
	files_hierarchy_window = {true};
	gameobjects_hierarchy_window = {true};
	loaded_objects_hierarchy_window = { true };

	map_editor_state = { false };

	color_picker = { false };
}

void RemoveWordFromLine(std::string& line, const std::string& word)
{
	auto n = line.find(word);
	if (n != std::string::npos)
	{
		line.erase(n, word.length());
	}
}

int findTexture(vector<SDL_Texture*> vec, TextureInfo* textureInfo) {
	int i;
	for (i = 0; i < vec.size(); i++) {
		if (vec.at(i) == textureInfo->texture) return i;
	}
	return NULL;
}

string convertVec2ToString(glm::vec2 vector,int round = 0) {
	return "[" + to_string(vector.x).substr(0, to_string(vector.x).find(".") + round) + ", " + to_string(vector.y).substr(0, to_string(vector.y).find(".") + round) + "]";
}

string convertVec3ToString(glm::vec3 vector, int round = 0) {
	return "[" + to_string(vector.x).substr(0, to_string(vector.x).find(".") + round) + ", " +
		to_string(vector.y).substr(0, to_string(vector.y).find(".") + round) + ", " +
		to_string(vector.z).substr(0, to_string(vector.z).find(".") + round) + "]";
}

void GUI::GameobjectsHierarchy() {
	string text;
	int id;
	
	string scene = systemm.GetScene();
	int size = systemm.entityManager.eList[scene]->size();
	ImGui::Begin("Scene objects",&gameobjects_hierarchy_window);

	auto items = systemm.GetScenes();
	static int item_current = 0;

	ImGui::Columns(2);

	string header_name = "Current Scene " + scene;

	ImGui::ListBoxHeader(header_name.c_str(), ImVec2(100, 50));
	for (int i = 0; i < items.size(); i++)
	{
		bool is_selected = (item_current == i);
		if (ImGui::Selectable(items[i].c_str(), is_selected)) {
			item_current = i;
			systemm.SetScene(items[i]);
		}
	}
	ImGui::ListBoxFooter();

	ImGui::NextColumn();

	//char scene_text_buff[16] = { 0 };
	ImGui::InputText("Type scene name", new_scene_input_buff, 16);
	if (ImGui::Button("Create scene")) {
		string name = string(new_scene_input_buff);
		systemm.SetScene(name);
		memset(new_scene_input_buff, 0, sizeof(new_scene_input_buff));
		systemm.SetScene(scene);
	}

	ImGui::Columns(1);
	for (int i = 0; i < size; i++) {
		id = systemm.entityManager.eList[scene]->at(i).id;
		text = "Entity: " + to_string(id);

		bool selected = (objectsParametersStatus.status == true && objectsParametersStatus.scene == scene && objectsParametersStatus.id == id);

		if (selected) ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(0, 255, 0, 255));
		auto button = ImGui::Button(text.c_str());
		if (selected) ImGui::PopStyleColor();

		if (button) { 
			objectsParametersStatus = { scene, true, id };
		};
	}
	ImGui::End();
}

glm::vec4 convertToRGB(float* colorBuf) {
	glm::vec4 color{ colorBuf[0] * 255,colorBuf[1] * 255,
		colorBuf[2] * 255, colorBuf[3] * 255 };
	return color;
}

float* convertToBuffer(float* colorBuf) {
	float buffer[4]{ colorBuf[0] * 255,colorBuf[1] * 255,
		colorBuf[2] * 255, colorBuf[3] * 255 };
	return buffer;
}

float* normalizeRGB_Float(glm::vec4 color) {
	float colorBuf[4] = { color.x / 255.0f,color.g / 255.0f ,color.b / 255.0f,color.a / 255.0 };
	return colorBuf;
}

glm::vec4 normalizeRGB_Vec4(glm::vec4 color) {
	glm::vec4 new_color = { color.x / 255.0f,color.g / 255.0f ,color.b / 255.0f,color.a / 255.0 };
	return new_color;
}


void GUI::ObjectsParameters(int id) {

	string save_scene = systemm.GetScene();
	systemm.SetScene(objectsParametersStatus.scene);

	string entityName, entityPos, entitySize, entityColor, entityMass
		,entityScale;
	Rigidbody* rg = systemm.component.GetComponent<Rigidbody>(id);
	Transform* transformValues = systemm.component.GetComponent<Transform>(id);

	string scene = systemm.GetScene();
	Entity* entity = &systemm.entityManager.eList[scene]->at(id);

	entityName = entity->name;/*"Entity: " + to_string(id);*/
	entityPos = "Position: " + convertVec2ToString(transformValues->pos, 3);
	entitySize = "Size: " + convertVec2ToString(transformValues->size, 2);
	entityMass = "Mass: " + to_string(transformValues->mass);
	entityColor = "Color: " + convertVec3ToString(transformValues->color);
	entityScale = "Scale: " + convertVec2ToString(transformValues->scale, 2);
	char nameBuf[32];
	strcpy(nameBuf, (char*)entity->name.c_str());	

	float* posBuf[2]{ &transformValues->pos.x, &transformValues->pos.y };
	float* sizeBuf[2]{ &transformValues->size.x, &transformValues->size.y };
	float* scaleBuf[2]{ &transformValues->scale.x, &transformValues->scale.y };
	float* colorBuf2[4]{ &transformValues->color.x, &transformValues->color.y, &transformValues->color.z,  &transformValues->color.a};
	float colorBuf1[4]{ transformValues->color.x / 255.0f, transformValues->color.y / 255.0f, transformValues->color.z / 255.0f,  transformValues->color.a / 255.0f };
	int keyboardInput = 0;

	ImGui::Begin("Object properties", &objectsParametersStatus.status);

	ImGui::Columns(2);
	if (ImGui::InputText("Entity name ", nameBuf, 30, 32)) {
		entity->name = nameBuf;
		//KEYBOARD_ACTIVE += 1;
	};

	ImGui::NextColumn();
	ImGui::Text(("Entity Desctiption:" + entity->description).c_str());
	ImGui::Separator();
	
	ImGui::Columns(1);
	keyboardInput += ImGui::InputFloat2("Entity position", *posBuf, "%.2f", 0 | 1);
	ImGui::Separator();

	keyboardInput += ImGui::InputFloat2("Entity size", *sizeBuf, "%.2f", 0 | 1);
	ImGui::Separator();

	keyboardInput += ImGui::InputFloat2("Entity scale", *scaleBuf, "%.2f", 0 | 1);
	ImGui::Separator();

	ImGui::Columns(2);
	if (ImGui::InputFloat4("Entity color", *colorBuf2, "%.f", 0 | 1)) {
		keyboardInput += 1;

	}
	ImGui::NextColumn();
	if (ImGui::Button("Color picker"))
	{
		if (color_picker == true) color_picker = false;
		else color_picker = true;
	}
	ImGui::Columns(1);
	if (color_picker) {
		if (ImGui::ColorEdit4("Color picker", colorBuf1)) {
			transformValues->color = convertToRGB(colorBuf1);
		}
	}
	ImGui::Separator();
	
	ImGui::Columns(1);
	ImGui::ListBoxHeader("Entity texture");

	int loaded_textures = systemm.transformComponent.textures.size();
	vector<SDL_Texture*> tex;
	for (int i = 0; i < loaded_textures; i++) {
		tex.push_back(systemm.transformComponent.textures.at(i).texture);
	}
	int i = 0;
	int index;
	for (auto item : systemm.transformComponent.textures) {
		//int index = (findTexture(tex, transformValues->texture_info) == i);
		if (transformValues->texture_info->texture_name == "") index = 0;
		else index = (findTexture(tex, transformValues->texture_info) == i);
		if (ImGui::Selectable(item.texture_name.c_str(),index)) {
			if (transformValues->texture_info != NULL && transformValues->texture_info->texture == item.texture) { transformValues->texture_info->texture = NULL; transformValues->texture_info->texture_name = ""; }
			else { 
				TextureInfo* texture_info = new TextureInfo;
				texture_info->texture = item.texture;  
				texture_info->texture_name = item.texture_name; 
				transformValues->texture_info = texture_info;
				
			}
		}
		i++;
	}
	ImGui::ListBoxFooter();
	ImGui::Separator();

	if (rg) {
		ImGui::NextColumn();
		ImGui::Text(to_string(rg->masno).c_str());
		ImGui::NextColumn();
	}

	ImGui::Columns(3);
	
	if (ImGui::Button("copy object")) {
		systemm.CreateCopyOfEntity(id, save_scene);
		systemm.SetScene(save_scene);

		SDL_Rect viewport;
		SDL_RenderGetViewport(renderer, &viewport);
		  
		Transform* tr = systemm.component.GetComponent<Transform>(systemm.entityManager.eAmount[save_scene] - 1);
		glm::vec2 viewport_center, object_center;

		viewport_center = glm::vec2(viewport.w, viewport.h) / 2.0f;
		object_center = tr->size / 2.0f;

		tr->pos = glm::vec2(0, 0) - object_center + viewport_center - systemm.camera.GetCameraOffset();
		
		systemm.SetScene(scene);
	}

	ImGui::NextColumn();
	if (ImGui::Button("save object")) {
		fManager.SaveFile(id, entity->name);

	}
	ImGui::NextColumn();
	if (ImGui::Button("delete object")) {
		systemm.DeleteEntity(id);
		if (systemm.entityManager.eList[scene]->size() > 0) {
			if (id > 0) objectsParametersStatus.id -= 1;
		}
		else objectsParametersStatus.status = false;
	}

	if (keyboardInput > 0) KEYBOARD_ACTIVE = false;

	ImGui::End();
	systemm.SetScene(save_scene);

}

void GUI::FilesHierarchy() {
	filesystem::path file_path;
	string file_name;

	ImGui::Begin("Hierarchy work in progress",&files_hierarchy_window);
	
	bool entry, exit;
	ImVec4 color;

	exit = ImGui::Button("...");
	if (exit) {
		string path = current_path.string();
		string word = "\\" + current_path.filename().string();
		RemoveWordFromLine(path, word);
		current_path = path;
	}
	for (const auto& dirEntry : filesystem::directory_iterator(current_path)) {
		file_path = dirEntry.path();
		file_name = dirEntry.path().filename().string();

		if (dirEntry.is_directory()) {
			color = { 100,200,100,255 };
		}
		else color = { 200,50,100,255 };

		ImGui::PushStyleColor(ImGuiCol_Text, IM_COL32(color.x,color.y,color.z,color.w));
		entry = ImGui::Button(file_name.c_str());
		ImGui::PopStyleColor();

		if (entry) {
			if (dirEntry.is_directory()) {
				current_path += "\\" + file_name;
			}
			else {
				if (file_path.extension() == ".jebacflorczyka") fManager.OpenFile(file_path);
			}
		}
	}
	ImGui::End();
}

void GUI::LoadedGameobjectsHierarchy() {
	filesystem::path file_path;
	ImGui::Begin("Objects", &loaded_objects_hierarchy_window);

	ImVec2 vMin = ImGui::GetWindowContentRegionMin();
	ImVec2 vMax = ImGui::GetWindowContentRegionMax();
	vMin.x += ImGui::GetWindowPos().x;
	vMin.y += ImGui::GetWindowPos().y;
	vMax.x += ImGui::GetWindowPos().x;
	vMax.y += ImGui::GetWindowPos().y;

	glm::vec2 offset = glm::vec2(vMin.x,vMin.y+50);
	glm::vec2 label_pos;
	glm::vec2 image_pos;

	glm::vec4 color;

	string saved_scene = systemm.GetScene();
	systemm.SetScene("GUI SCENE");

	glm::vec2 grid_size = glm::vec2(100, 60);

	if (ImGui::Button("Refresh", ImVec2(60, 26))) { 
		fManager.LoadGameobjects(); 
	}

	ImDrawList* draw_list = ImGui::GetWindowDrawList();

	for (const auto& entity : *systemm.entityManager.eList[systemm.GetScene()]) {
		Transform* transform = systemm.component.GetComponent<Transform>(entity.id);
		if (offset.x > vMax.x) { 
			offset.y += grid_size.y; 
			offset.x = vMin.x;
		}
		image_pos = offset;
		label_pos = offset - glm::vec2(vMin.x,vMin.y);

		if (transform->texture_info->texture_name != "") {
			ImGui::SetCursorPos(ImVec2(image_pos.x - vMin.x + 10 , image_pos.y + transform->size.y - vMin.y - 5));
			//ImGui::Image(transform->texture_info->texture, ImVec2(32, 32));
			if (ImGui::ImageButton(transform->texture_info->texture, ImVec2(32, 32), ImVec2(0.0f, 0.0f), ImVec2(1.0f, 1.0f), 0)) {
				objectsParametersStatus = { systemm.GetScene(), true, entity.id };
			};
		}
		else {
			color = normalizeRGB_Vec4(transform->color);
			ImColor imcolor{ color.r,color.g,color.b,color.a };
			ImVec2 cursor_pos = ImGui::GetCursorScreenPos();
			draw_list->AddRectFilled(ImVec2(image_pos.x, image_pos.y), ImVec2(image_pos.x + transform->size.x, image_pos.y + transform->size.y), imcolor, 0.0f, 0);
			
			int mouse_x;
			int mouse_y;

			SDL_GetMouseState(&mouse_x, &mouse_y);

			bool is_clicked = ImGui::IsMouseReleased(0) && ImGui::IsMouseHoveringRect(ImVec2(image_pos.x,image_pos.y), ImVec2(image_pos.x + transform->size.x, image_pos.y + transform->size.y));
			if(is_clicked) objectsParametersStatus = { systemm.GetScene(), true, entity.id };
		}
		ImGui::SetCursorPos(ImVec2(label_pos.x + 5 ,label_pos.y + transform->size.y * 2));
		ImGui::Text(entity.name.c_str());
		offset.x += grid_size.x;
	}
	systemm.SetScene(saved_scene);
	ImGui::End();
}

void GUI::Scene() {
	ImGui::GetStyle().WindowMinSize = ImVec2(600, 500);
	ImGui::Begin("Scene", &scene_window);

	ImVec2 vMin = ImGui::GetWindowContentRegionMin();
	ImVec2 vMax = ImGui::GetWindowContentRegionMax();
	vMin.x += ImGui::GetWindowPos().x;
	vMin.y += ImGui::GetWindowPos().y;
	vMax.x += ImGui::GetWindowPos().x;
	vMax.y += ImGui::GetWindowPos().y;

	SDL_Rect rect{ vMin.x,vMin.y,vMax.x - vMin.x ,vMax.y - vMin.y };
	SDL_RenderSetViewport(renderer, &rect);

	SDL_Rect viewport;
	SDL_RenderGetViewport(renderer, &viewport);
	SDL_GetMouseState(&mousex, &mousey);


	mousex -= viewport.x;
	mousey -= viewport.y;

	if (map_editor_state) {
		mEditor.SetGridSize(glm::vec2(32,32));
		mEditor.CreateGrid(viewport);
		mEditor.SetGridOffset(systemm.camera.GetCameraOffset());
		mEditor.CorrectMousePos(mousex, mousey);
	}
	ImGui::End();
}

void GUI::MenuBar() {

	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("Project"))
		{
			if (ImGui::MenuItem("Open")) {  }
			if (ImGui::MenuItem("Save")) {  }
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Windows"))
		{
			if (ImGui::MenuItem("Files hierarchy")) files_hierarchy_window = true;
			if (ImGui::MenuItem("Objects hierarchy")) gameobjects_hierarchy_window = true;
			if (ImGui::MenuItem("Scene window")) scene_window = true;
			ImGui::EndMenu();
		}

		if (ImGui::BeginMenu("Map editor"))
		{
			if (ImGui::MenuItem("Enable grid")) {
				map_editor_state = true;
			}
			if (ImGui::MenuItem("Disable grid")) {
				map_editor_state = false;
			}
			ImGui::EndMenu();
		}

		ImGui::EndMainMenuBar();
	}
}

void GUI::UpdateGui() {
	MenuBar();
	if (scene != systemm.GetScene()) {
		objectsParametersStatus.status = false;
		scene = systemm.GetScene();
	}
	if (gameobjects_hierarchy_window) GameobjectsHierarchy();
	if (objectsParametersStatus.status) ObjectsParameters(objectsParametersStatus.id);
	if (files_hierarchy_window ) FilesHierarchy();
	if (scene_window) Scene();
	if (loaded_objects_hierarchy_window) LoadedGameobjectsHierarchy();
}



