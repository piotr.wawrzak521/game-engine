#include "EntityManagerClass.h"

EntityManager::EntityManager() {
	scene = "0";
	eAmount = unordered_map<string, int>();
	eAmount[scene] = 0;
	eList = unordered_map<string, vector<Entity>*>();
	eList[scene] = new vector<Entity>();
}

void EntityManager::CorrectValues(const int id) {
	for (int i = 0; i < eList[scene]->size(); i++) {
		if (i > id) {
			eList[scene]->at(i).id -= 1;
		}
	}
}

void EntityManager::AddEntity() {
	Entity entity;
	entity.id = eAmount[scene];
	entity.name = "Entity " + to_string(eAmount[scene]);
	eList[scene]->push_back(entity);
	eAmount[scene]++;
}

void EntityManager::DeleteEntity(const int id) {
	CorrectValues(id);
	eList[scene]->erase(eList[scene]->begin() + id);
	eAmount[scene]--;
}

void EntityManager::CopyEntity(const int id, string selected_scene) {
	//Entity entity;
	Entity entity;
	entity.id = eAmount[selected_scene];
	entity.name = "Entity " + to_string(eAmount[selected_scene]);
	entity.description = "Copy of entity: " + to_string(id);
	eList[selected_scene]->push_back(entity);
	eAmount[selected_scene]++;
}
void EntityManager::SetScene(const string new_scene) {
	scene = new_scene;
	if(eList[scene] == NULL) eList[scene] = new vector<Entity>();
}

Entity* EntityManager::GetEntity(int id) {
	return &eList[scene]->at(id);
}

void EntityManager::PrintData() {
	for (int i = 0; i < eList[scene]->size(); i++) {
		cout << GetEntity(i)->id << GetEntity(i)->description << endl;
	}
}

void EntityManager::DeleteEntitiesFromScene(const string scene){
	eList[scene]->clear();
	eAmount[scene] = 0;
}
